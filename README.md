# Rust Setup

## Description

This project serves as a reminder of where to start with rust and how to quickly setup an environment.

## Getting started

First here couple of mendatory and recommended links to make it easy for you to get started with Rust:

* [Rust language](https://www.rust-lang.org/)
* [Rust Learn](https://www.rust-lang.org/learn)
  * [The Rust Programming Language](https://doc.rust-lang.org/book/) 
* [The Rust Standard Library](https://doc.rust-lang.org/std/)

## VSCode Rust Setup

1. Install rust with [rustup.rs](https://rustup.rs/) and follow the instruction there.
    1. For installation details [Rust Installation](https://doc.rust-lang.org/book/ch01-01-installation.html)
2. Install [Visual Studio Code](https://code.visualstudio.com/) (not to be confused with Visual Studio).
3. Install the following VSCode extensions:
    1. [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=matklad.rust-analyzer)
    2. [vadimcn.vscode-lldb](https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb)
    3. [serayuzgur.crates](https://marketplace.visualstudio.com/items?itemName=serayuzgur.crates)
    4. [tamasfe.even-better-toml](https://marketplace.visualstudio.com/items?itemName=tamasfe.even-better-toml)
4. In addition, those extensions are also quite useful:
    1. [change-string-case](https://marketplace.visualstudio.com/items?itemName=maximus136.change-string-case)
    2. [code-spell-checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
    3. [Markdown Preview Mermaid Support](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid)
    4. [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
5. Change VSCode configuration:
    1. Settings -> Text Editor -> Formatting -> Format On Save
        1. ==> **Make sure it's checked!**
    2. Settings -> Extensions -> Rust Analyzer -> Check On Save: Command
       1. ==> **Replace `check` by `clippy`**. `clippy` is the Rust linter and will report useful warnings for better code quality
6. Profit!

## Contributor

Special thanks to Nicolas Bigaouette for his Rust passion and more:

 * [GitLab](https://gitlab.com/nbigaouette1) 
 * [GitHub](https://github.com/nbigaouette)
